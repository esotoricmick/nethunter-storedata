* OpenSSL 1.1.1a with TLS 1.3, expect some breakage on weird or uncommon TLS configurations!
* VORACLE mitigation, compression defaults to asymmetric if enabled
* IPv6 only VPN support
* OpenVPN 3 core switched to OpenSSL 1.1.1a
* External PKI support (alpha)
* Use -O0 for lzo on armv7a to workaround compiler breakage
